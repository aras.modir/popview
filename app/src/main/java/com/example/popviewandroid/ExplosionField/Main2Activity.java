package com.example.popviewandroid.ExplosionField;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.popviewandroid.R;

import tyrantgit.explosionfield.ExplosionField;

public class Main2Activity extends AppCompatActivity {
    private RelativeLayout relativeLayout;
    private ImageView imageView;
    private ExplosionField explosionField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        relativeLayout = findViewById(R.id.relativeLayout2);
        imageView = findViewById(R.id.image2);

        explosionField = ExplosionField.attach2Window(this);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                explosionField.explode(v);

            }
        });


    }
}
