package com.example.popviewandroid;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.popviewandroid.PopView.PopField;

public class MainActivity extends AppCompatActivity {

    private RelativeLayout relativeLayout;
    private ImageView imageView;
    private ImageView imageView1;
    private PopField popField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        popField = PopField.attach2Window(this);

        relativeLayout = findViewById(R.id.relativeLayout);
        imageView = findViewById(R.id.image);
        imageView1 = findViewById(R.id.sample_image);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popField.popView(relativeLayout);

            }
        });

//        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        @SuppressLint("InflateParams") final View addView = layoutInflater.inflate(R.layout.samplelayout, null);         //Inflate new view from xml
//        ImageView newTextView = (ImageView) addView.findViewById(R.id.sample_image);    //Reference the newview
//
//        popField.popView(relativeLayout, addView, true);

    }
}
